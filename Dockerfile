FROM python:3.10-slim

WORKDIR app
COPY . ./

RUN ./install_packages.sh && \
      pip install -r requirements.txt && \
      ./init_db.py && \
      (test -f data/Dijon_sensorSQLite20220530.db && rm data/Dijon_sensorSQLite20220530.db || true) && \
      (test -f data/Ishinomaki_DB.csv && rm data/Ishinomaki_DB.csv)

EXPOSE 8000

CMD ["uvicorn", "--host", "0.0.0.0", "main:app"]

import unittest

from sqlalchemy import create_engine, inspect
from sqlalchemy.event import listen
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import select, func

from database import crud, models, schemas
from database.engine import Base, load_spatialite

TEST_USER_ID = 1234
TEST_USER_NAME = 'TestUser'
TEST_USER_PASSWORD = 'password'

TEST_COVERAGE_ID = 1234
TEST_COVERAGE_NAME = 'TEST_COVERAGE'


class TestDatabase(unittest.TestCase):

    def setUp(self):
        SQLALCHEMY_DATABASE_URL = 'sqlite:///:memory:'

        self.engine = create_engine(
            SQLALCHEMY_DATABASE_URL, connect_args={'check_same_thread': False}
        )
        # setup spatialite
        listen(self.engine, 'connect', load_spatialite)
        conn = self.engine.connect()
        try:
            conn.execute(select([func.InitSpatialMetaData()]))
        except Exception:
            pass
        finally:
            conn.close()

        self.SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=self.engine)
        Base.metadata.create_all(bind=self.engine)

        self.db = self.SessionLocal()

    def create_test_user(self):
        db_user = models.User(id=TEST_USER_ID, name=TEST_USER_NAME, password=TEST_USER_PASSWORD)
        self.db.add(db_user)
        self.db.commit()
        self.db.refresh(db_user)
        return db_user

    def create_test_coverage(self):
        db_coverage = models.Coverage(id=TEST_COVERAGE_ID, name=TEST_COVERAGE_NAME)
        self.db.add(db_coverage)
        self.db.commit()
        self.db.refresh(db_coverage)
        return db_coverage

    def get_test_user(self):
        return self.db.query(models.User).filter(models.User.id == TEST_USER_ID).first()

    def get_test_coverage(self):
        return \
            self.db.query(models.Coverage).filter(models.Coverage.id == TEST_COVERAGE_ID).first()

    def test_table_users_exist(self):
        self.assertTrue(inspect(self.engine).has_table('users'))

    def test_create_user(self):
        user = schemas.UserCreate(name=TEST_USER_NAME, password=TEST_USER_PASSWORD)
        db_user = crud.create_user(self.db, user)
        self.assertIsNotNone(db_user.id)

        with self.assertRaises(IntegrityError):
            crud.create_user(self.db, user)

    def test_get_user(self):
        db_user = self.create_test_user()

        res_user = crud.get_user(self.db, TEST_USER_ID)
        self.assertEqual(db_user, res_user)

    def test_get_user_by_name(self):
        self.create_test_user()

        db_user = crud.get_user_by_name(self.db, TEST_USER_NAME)
        self.assertEqual(db_user.name, TEST_USER_NAME)

    def test_get_users(self):
        users = crud.get_users(self.db)

        self.assertEqual(type(users), list)
        self.assertEqual(len(users), 0)

        self.create_test_user()

        users = crud.get_users(self.db)

        self.assertEqual(type(users), list)
        self.assertEqual(len(users), 1)

    def test_add_coverage_to_user(self):
        db_coverage = self.create_test_coverage()
        db_user = self.create_test_user()

        self.assertEqual(len(db_user.coverages), 0)

        crud.add_coverage_to_user(self.db, db_user, db_coverage)

        self.assertEqual(len(db_user.coverages), 1)

        self.assertIn(db_coverage, db_user.coverages)

    def test_delete_user(self):
        self.create_test_user()

        count = crud.delete_user(self.db, TEST_USER_ID)

        self.assertEqual(count, 1)

        db_user = self.get_test_user()
        self.assertIsNone(db_user)

    def test_create_coverage(self):
        coverage = schemas.CoverageCreate()
        db_coverage = crud.create_coverage(self.db, TEST_COVERAGE_NAME, coverage)
        db_coverage = self.assertIsNotNone(db_coverage.id)

        with self.assertRaises(IntegrityError):
            crud.create_coverage(self.db, TEST_COVERAGE_NAME, coverage)

    def test_get_coverage(self):
        db_coverage = self.create_test_coverage()

        res_coverage = crud.get_coverage(self.db, db_coverage.id)
        self.assertEqual(res_coverage, db_coverage)

    def test_get_coverage_by_name(self):
        db_coverage = self.create_test_coverage()

        res_coverage = crud.get_coverage_by_name(self.db, TEST_COVERAGE_NAME)
        self.assertEqual(db_coverage, res_coverage)

    def test_get_coverages(self):
        coverages = crud.get_coverages(self.db)

        self.assertEqual(type(coverages), list)
        self.assertEqual(len(coverages), 0)

        self.create_test_coverage()

        coverages = crud.get_coverages(self.db)

        self.assertEqual(type(coverages), list)
        self.assertEqual(len(coverages), 1)

    def test_delete_coverage(self):
        self.create_test_coverage()

        count = crud.delete_coverage_by_name(self.db, TEST_COVERAGE_NAME)

        self.assertEqual(count, 1)

        db_coverage = self.get_test_coverage()
        self.assertIsNone(db_coverage)

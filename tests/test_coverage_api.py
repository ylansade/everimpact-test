import os
import unittest

from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.event import listen
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import select, func

# we need to import models so that we create the tables in the test database
from database import models  # noqa
from database.engine import Base, load_spatialite
from init_db import init_test_db
from main import app
from utils.dependencies import get_db

client = TestClient(app)

SQLALCHEMY_DATABASE_URL = 'sqlite:///test.db'

WORKING_POLYGON = """{"type": "polygon", "coordinates": [[[-100, -100], [100, -100], [100, 100],
                                                          [-100, 100], [-100, -100]]]}"""
NOT_WORKING_POLYGON = """{"type": "polygon", "coordinates": [[[0, 0], [1, 0], [1, 1],
                                                              [0, 1], [0, 0]]]}"""


class TestUsersApi(unittest.TestCase):

    def setUp(self):
        if os.path.exists('test.db'):
            os.remove('test.db')
        self.engine = create_engine(
            SQLALCHEMY_DATABASE_URL, connect_args={'check_same_thread': False}
        )
        # setup spatialite
        listen(self.engine, 'connect', load_spatialite)
        conn = self.engine.connect()
        try:
            conn.execute(select([func.InitSpatialMetaData()]))
        except Exception:
            pass
        finally:
            conn.close()

        SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=self.engine)
        Base.metadata.create_all(bind=self.engine)
        db = SessionLocal()
        init_test_db(db)

        def override_get_db():
            db = SessionLocal()
            try:
                yield db
            finally:
                db.close()

        app.dependency_overrides[get_db] = override_get_db

        self.admin_auth = ('admin', 'admin')
        self.user_auth = ('dijon', 'dijon')
        self.user_id = 2
        self.user_coverage = 'Dijon'
        self.sink_auth = ('ishinomaki', 'ishinomaki')

    def test_create_coverage(self):
        coverage = {'spatial_resolution': 1.1, 'temporal_resolution': 2.2, 'depth': 3.3}
        name = 'TEST'

        # test creation successful
        r = client.post(f'/coverage/{name}', json=coverage, auth=self.admin_auth)
        self.assertEqual(r.status_code, 200)

        # test unauthorized if not admin
        r = client.post(f'/coverage/{name}', json=coverage, auth=self.user_auth)
        self.assertEqual(r.status_code, 401)

        # test failure when creating duplicate
        r = client.post(f'/coverage/{name}', json=coverage, auth=self.admin_auth)
        self.assertEqual(r.status_code, 400)

    def test_get_all_coverages(self):
        # test unauthenticated
        r = client.get('/coverage')
        self.assertEqual(r.status_code, 401)

        # test admin result
        r = client.get('/coverage', auth=self.admin_auth)
        self.assertEqual(r.status_code, 200)

        coverages = r.json()
        self.assertEqual(len(coverages), 2)

        # test user result
        r = client.get('/coverage', auth=self.user_auth)

        self.assertEqual(r.status_code, 200)

        coverages = r.json()
        self.assertEqual(len(coverages), 1)
        self.assertEqual(coverages[0]['name'], self.user_coverage)

    def test_filter_sensors(self):
        # test unauthorized
        r = client.get('/coverage/Dijon/sensors/filter', auth=self.sink_auth)
        self.assertEqual(r.status_code, 401)

        # test authorized
        r = client.get('/coverage/Dijon/sensors/filter', auth=self.user_auth)
        self.assertEqual(r.status_code, 200)

        readings = r.json()
        self.assertEqual(len(readings), 999)

        # test admin
        r = client.get('/coverage/Dijon/sensors/filter', auth=self.admin_auth)
        self.assertEqual(r.status_code, 200)

        # test date begin
        r = client.get('/coverage/Dijon/sensors/filter', auth=self.user_auth,
                       params={'date_begin': '202401010000'})
        self.assertEqual(r.status_code, 200)

        readings = r.json()
        self.assertEqual(len(readings), 0)

        # test date end
        r = client.get('/coverage/Dijon/sensors/filter', auth=self.user_auth,
                       params={'date_end': '200001010000'})
        self.assertEqual(r.status_code, 200)

        readings = r.json()
        self.assertEqual(len(readings), 0)

        # test polygon
        r = client.get('/coverage/Dijon/sensors/filter', auth=self.user_auth,
                       params={'polygon': WORKING_POLYGON})

        self.assertEqual(r.status_code, 200)

        readings = r.json()
        self.assertEqual(len(readings), 999)

        r = client.get('/coverage/Dijon/sensors/filter', auth=self.user_auth,
                       params={'polygon': NOT_WORKING_POLYGON})

        self.assertEqual(r.status_code, 200)

        readings = r.json()
        self.assertEqual(len(readings), 0)

    def test_filter_sinks(self):
        # test unauthorized
        r = client.get('/coverage/Ishinomaki/sinks/filter', auth=self.user_auth)
        self.assertEqual(r.status_code, 401)

        # test authorized
        r = client.get('/coverage/Ishinomaki/sinks/filter', auth=self.sink_auth)
        self.assertEqual(r.status_code, 200)

        sinks = r.json()
        self.assertEqual(len(sinks), 99)

        # test admin
        r = client.get('/coverage/Ishinomaki/sinks/filter', auth=self.admin_auth)
        self.assertEqual(r.status_code, 200)

        # test date begin
        r = client.get('/coverage/Ishinomaki/sinks/filter', auth=self.admin_auth,
                       params={'date_begin': '202401010000'})
        self.assertEqual(r.status_code, 200)

        sinks = r.json()
        self.assertEqual(len(sinks), 0)

        # test date end
        r = client.get('/coverage/Ishinomaki/sinks/filter', auth=self.admin_auth,
                       params={'date_end': '200001010000'})
        self.assertEqual(r.status_code, 200)

        sinks = r.json()
        self.assertEqual(len(sinks), 49)

        # test specie
        r = client.get('/coverage/Ishinomaki/sinks/filter', auth=self.admin_auth,
                       params={'specie': 'enf'})
        self.assertEqual(r.status_code, 200)

        # test age begin
        r = client.get('/coverage/Ishinomaki/sinks/filter', auth=self.admin_auth,
                       params={'age_begin': 85})
        self.assertEqual(r.status_code, 200)

        sinks = r.json()
        self.assertEqual(len(sinks), 49)

        # test age end
        r = client.get('/coverage/Ishinomaki/sinks/filter', auth=self.admin_auth,
                       params={'age_end': 85})
        self.assertEqual(r.status_code, 200)

        sinks = r.json()
        self.assertEqual(len(sinks), 50)

        # test polygon
        r = client.get('/coverage/Ishinomaki/sinks/filter', auth=self.admin_auth,
                       params={'polygon': NOT_WORKING_POLYGON})
        self.assertEqual(r.status_code, 200)

        sinks = r.json()
        self.assertEqual(len(sinks), 0)

    def test_delete_coverage(self):
        # test unauthorized if not admin
        r = client.delete(f'/coverage/{self.user_coverage}', auth=self.user_auth)
        self.assertEqual(r.status_code, 401)

        r = client.delete(f'/coverage/{self.user_coverage}', auth=self.admin_auth)
        self.assertEqual(r.status_code, 200)

        r = client.delete(f'/coverage/{self.user_coverage}', auth=self.admin_auth)
        self.assertEqual(r.status_code, 404)

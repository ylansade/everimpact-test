from datetime import datetime
import unittest

import geojson

from shapely.geometry import shape
from sqlalchemy import create_engine
from sqlalchemy.event import listen
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import select, func
from geoalchemy2 import WKTElement

from database import crud, models
from database.engine import Base, load_spatialite
from init_db import import_coverages, import_sensors

TEST_USER_ID = 1234
TEST_USER_NAME = 'TestUser'
TEST_USER_PASSWORD = 'password'

TEST_COVERAGE_ID = 1234
TEST_COVERAGE_NAME = 'TEST_COVERAGE'

DIJON_COVERAGE_ID = 1
ISHINOMAKI_COVERAGE_ID = 2

dijon_communes = geojson.load(open('data/dijon_communes.geojson'))

WORKING_POLYGON = """{"type": "polygon", "coordinates": [[[-100, -100], [100, -100], [100, 100],
                                                          [-100, 100], [-100, -100]]]}"""
NOT_WORKING_POLYGON = """{"type": "polygon", "coordinates": [[[0, 0], [1, 0], [1, 1],
                                                              [0, 1], [0, 0]]]}"""


class TestSpatialite(unittest.TestCase):

    def setUp(self):
        SQLALCHEMY_DATABASE_URL = 'sqlite:///:memory:'

        self.engine = create_engine(
            SQLALCHEMY_DATABASE_URL, connect_args={'check_same_thread': False}
        )
        # setup spatialite
        listen(self.engine, 'connect', load_spatialite)
        conn = self.engine.connect()
        try:
            conn.execute(select([func.InitSpatialMetaData()]))
        except Exception:
            pass
        finally:
            conn.close()

        self.SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=self.engine)
        Base.metadata.create_all(bind=self.engine)

        self.db = self.SessionLocal()

        import_coverages(self.db, 'data/coverages.csv')

    def create_test_sensor(self):
        sensor = models.Sensor(geom='POINT(1 1 0)', coverage_id=DIJON_COVERAGE_ID)
        self.db.add(sensor)
        self.db.commit()
        self.db.refresh(sensor)
        return sensor

    def create_test_sinks(self):
        self.db.add_all([
            models.Sink(
                coverage_id=ISHINOMAKI_COVERAGE_ID,
                parcel_id='1',
                date=datetime.strptime('202201010000', '%Y%m%d%H%S'),
                specie='oak',
                age=10,
                co2balance=0,
                co2emitted=0,
                co2removed=0,
                geom='POLYGON((0 0, 2 0, 2 2, 0 2, 0 0))'),
            models.Sink(
                coverage_id=ISHINOMAKI_COVERAGE_ID,
                parcel_id='2',
                date=datetime.strptime('202301010000', '%Y%m%d%H%S'),
                specie='pine',
                age=20,
                co2balance=0,
                co2emitted=0,
                co2removed=0,
                geom='POLYGON((0 0, -2 0, -2 -2, 0 -2, 0 0))'
            )
        ])
        self.db.commit()

    def test_create_sensor(self):
        self.create_test_sensor()

        sensor = self.db.query(models.Sensor).filter(
            func.ST_Equals(models.Sensor.geom, WKTElement('POINT(1 1 0)'))).first()
        self.assertIsNotNone(sensor)

        sensor = self.db.query(models.Sensor).filter(
            func.ST_Equals(models.Sensor.geom, WKTElement('POINT(-1 2)'))).first()
        self.assertIsNone(sensor)

    def test_find_sensors_by_geom(self):
        self.create_test_sensor()

        query = self.db.query(models.Sensor).filter(
            func.ST_Contains(WKTElement('POLYGON((0 0, 2 0, 2 2, 0 2, 0 0))'),
                             models.Sensor.geom)
        ).all()
        self.assertEqual(len(query), 1)

        polygon = WKTElement('POLYGON((0 0, 2 0, 2 2, 0 2, 0 0))')
        sensors = crud.find_sensors_by_geom(self.db, DIJON_COVERAGE_ID, polygon)
        self.assertEqual(len(sensors), 1)

        polygon = WKTElement('POLYGON((0 0, -2 0, -2 -2, 0 -2, 0 0))')
        sensors = crud.find_sensors_by_geom(self.db, DIJON_COVERAGE_ID, polygon)
        self.assertEqual(len(sensors), 0)

    def test_find_sensors_using_real_data(self):
        import_sensors(self.db, 'data/sensors_locations.geojson')

        commune_geom = shape(dijon_communes['features'][0]['geometry'])
        sensors = crud.find_sensors_by_geom(self.db, DIJON_COVERAGE_ID,
                                            WKTElement(commune_geom.wkt))
        self.assertEqual(len(sensors), 1)

        commune_geom = shape(dijon_communes['features'][1]['geometry'])
        sensors = crud.find_sensors_by_geom(self.db, DIJON_COVERAGE_ID,
                                            WKTElement(commune_geom.wkt))
        self.assertEqual(len(sensors), 36)

        polygon = geojson.loads(WORKING_POLYGON)
        polygon_geom = shape(polygon)
        sensors = crud.find_sensors_by_geom(self.db, DIJON_COVERAGE_ID,
                                            WKTElement(polygon_geom.wkt))
        self.assertEqual(len(sensors), 75)

        polygon = geojson.loads(NOT_WORKING_POLYGON)
        polygon_geom = shape(polygon)
        sensors = crud.find_sensors_by_geom(self.db, DIJON_COVERAGE_ID,
                                            WKTElement(polygon_geom.wkt))
        self.assertEqual(len(sensors), 0)

    def test_find_sinks(self):
        self.create_test_sinks()

        # no filter
        sinks = crud.find_sinks(self.db, ISHINOMAKI_COVERAGE_ID)
        self.assertEqual(len(sinks), 2)

        # date filters
        sinks = crud.find_sinks(self.db, ISHINOMAKI_COVERAGE_ID,
                                date_begin=datetime(2022, 1, 2))
        self.assertEqual(len(sinks), 1)
        self.assertEqual(sinks[0].parcel_id, '2')

        sinks = crud.find_sinks(self.db, ISHINOMAKI_COVERAGE_ID,
                                date_end=datetime(2022, 1, 2))
        self.assertEqual(len(sinks), 1)

        sinks = crud.find_sinks(self.db, ISHINOMAKI_COVERAGE_ID,
                                date_begin=datetime(2022, 1, 2),
                                date_end=datetime(2022, 1, 3))
        self.assertEqual(len(sinks), 0)

        # specie filter
        sinks = crud.find_sinks(self.db, ISHINOMAKI_COVERAGE_ID,
                                specie='oak')
        self.assertEqual(len(sinks), 1)
        self.assertEqual(sinks[0].parcel_id, '1')

        # age filters
        sinks = crud.find_sinks(self.db, ISHINOMAKI_COVERAGE_ID,
                                age_begin=15)
        self.assertEqual(len(sinks), 1)
        self.assertEqual(sinks[0].parcel_id, '2')

        sinks = crud.find_sinks(self.db, ISHINOMAKI_COVERAGE_ID,
                                age_end=15)
        self.assertEqual(len(sinks), 1)
        self.assertEqual(sinks[0].parcel_id, '1')

        # polygon filter
        polygon = WKTElement('POLYGON((-1 1, 1 1, 1 -1, -1 -1))')
        sinks = crud.find_sinks(self.db, ISHINOMAKI_COVERAGE_ID,
                                geom=polygon)
        self.assertEqual(len(sinks), 2)

        polygon = WKTElement('POLYGON((3 3, 3 4, 4 4, 3 4, 3 3))')
        sinks = crud.find_sinks(self.db, ISHINOMAKI_COVERAGE_ID,
                                geom=polygon)
        self.assertEqual(len(sinks), 0)

import os
import unittest

from fastapi.testclient import TestClient
from sqlalchemy import create_engine, inspect
from sqlalchemy.event import listen
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import select, func

# we need to import models so that we create the tables in the test database
from database import models  # noqa
from database.engine import Base, load_spatialite
from init_db import init_test_db
from main import app
from utils.dependencies import get_db

client = TestClient(app)

SQLALCHEMY_DATABASE_URL = 'sqlite:///test.db'


class TestUsersApi(unittest.TestCase):

    def setUp(self):
        if os.path.exists('test.db'):
            os.remove('test.db')
        self.engine = create_engine(
            SQLALCHEMY_DATABASE_URL, connect_args={'check_same_thread': False}
        )
        # setup spatialite
        listen(self.engine, 'connect', load_spatialite)
        conn = self.engine.connect()
        try:
            conn.execute(select([func.InitSpatialMetaData()]))
        except Exception:
            pass
        finally:
            conn.close()

        SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=self.engine)
        Base.metadata.create_all(bind=self.engine)
        db = SessionLocal()
        init_test_db(db)

        def override_get_db():
            db = SessionLocal()
            try:
                yield db
            finally:
                db.close()

        app.dependency_overrides[get_db] = override_get_db

        self.admin_auth = ('admin', 'admin')
        self.user_auth = ('dijon', 'dijon')
        self.user_id = 2
        self.user_coverage = 'Dijon'
        self.user_coverage_id = 1

    def test_table_users_exist(self):
        self.assertTrue(inspect(self.engine).has_table('users'))

    def test_get_all_users(self):
        # test unauthorized without authentification
        r = client.get('/users/all')
        self.assertEqual(r.status_code, 401)

        # test working condition
        r = client.get('/users/all', auth=self.admin_auth)
        self.assertEqual(r.status_code, 200)

        users = r.json()
        self.assertEqual(type(users), list)
        self.assertEqual(len(users), 3)

        # test unauthorized if not admin
        r = client.get('/users/all', auth=self.user_auth)
        self.assertEqual(r.status_code, 401)

    def test_create_user(self):
        user = {'name': 'testuser', 'password': 'testuser'}

        r = client.post('/users', json=user, auth=self.admin_auth)
        self.assertEqual(r.status_code, 200)

        # test unauthorized if not admin
        r = client.post('/users', json=user, auth=self.user_auth)
        self.assertEqual(r.status_code, 401)

    def test_get_current_user(self):
        r = client.get('/users/me', auth=self.user_auth)
        self.assertEqual(r.status_code, 200)

        user = r.json()
        self.assertEqual(user['id'], self.user_id)
        self.assertEqual(user['name'], self.user_auth[0])

        coverages = user['coverages']
        self.assertEqual(len(coverages), 1)
        self.assertEqual(coverages[0]['name'], self.user_coverage)

    def test_update_current_user(self):
        data = {'action': 'remove_coverage', 'coverage_id': self.user_coverage_id}

        r = client.put('/users/me', json=data, auth=self.admin_auth)
        self.assertEqual(r.status_code, 200)

        user = r.json()
        self.assertEqual(user['coverages'], [])

        data['action'] = 'add_coverage'
        r = client.put('/users/me', json=data, auth=self.admin_auth)
        self.assertEqual(r.status_code, 200)

        user = r.json()
        self.assertEqual(len(user['coverages']), 1)
        self.assertEqual(user['coverages'][0]['id'], self.user_coverage_id)

    def test_update_user_by_id(self):
        data = {'action': 'remove_coverage', 'coverage_id': self.user_coverage_id}

        r = client.put(f'/users/{self.user_id}', json=data, auth=self.admin_auth)
        self.assertEqual(r.status_code, 200)

        user = r.json()
        self.assertEqual(user['coverages'], [])

        data['action'] = 'add_coverage'
        r = client.put(f'/users/{self.user_id}', json=data, auth=self.admin_auth)
        self.assertEqual(r.status_code, 200)

        user = r.json()
        self.assertEqual(len(user['coverages']), 1)
        self.assertEqual(user['coverages'][0]['id'], self.user_coverage_id)

    def test_delete_user(self):
        # test unauthorized if not admin
        r = client.delete(f'/users/{self.user_id}', auth=self.user_auth)
        self.assertEqual(r.status_code, 401)

        r = client.delete(f'/users/{self.user_id}', auth=self.admin_auth)
        self.assertEqual(r.status_code, 200)

        r = client.delete(f'/users/{self.user_id}', auth=self.admin_auth)
        self.assertEqual(r.status_code, 404)

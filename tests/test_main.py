import unittest

from fastapi.testclient import TestClient

from main import app

client = TestClient(app)


class TestMain(unittest.TestCase):

    def test_app_is_running(self):
        r = client.get('/docs')
        self.assertEqual(r.status_code, 200)

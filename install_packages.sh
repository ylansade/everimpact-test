#!/usr/bin/env sh

export DEBIAN_FRONTEND=noninteractive

apt-get update
apt-get -y upgrade
apt-get -y install --no-install-recommends libsqlite3-mod-spatialite

apt-get clean
rm -rf /var/lib/apt/lists/*

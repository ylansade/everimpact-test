from typing import Annotated

import bcrypt

from fastapi import Depends, HTTPException, status
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from sqlalchemy.orm import Session

from database import crud, schemas
from database.engine import SessionLocal

security = HTTPBasic()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def authenticate_current_user(credentials: Annotated[HTTPBasicCredentials, Depends(security)],
                              db: Session = Depends(get_db)):
    """ Authenticate current user against users in the database """
    db_user = crud.get_user_by_name(db, credentials.username)

    password = credentials.password.encode('utf8')

    if not db_user or not bcrypt.checkpw(password, db_user.password):
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                            detail='Incorrect credentials')

    return db_user


def authorize_administrator(user: Annotated[schemas.User, Depends(authenticate_current_user)]):
    """ Raise an error if the current user isn't an administrator """
    if not user.is_admin:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                            detail='Insufficient privileges')
    return user.is_admin


def authorize_coverage(name: str,
                       user: Annotated[schemas.User, Depends(authenticate_current_user)],
                       db: Session = Depends(get_db)):
    """ Get the coverage in parameter and make sure user is allowed to access it """
    coverage = crud.get_coverage_by_name(db, name)
    if not coverage:
        raise HTTPException(status_code=404,
                            detail=f'Coverage {name} was not found in the database')

    if coverage not in user.coverages and not user.is_admin:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                            detail=f"You're not allowed to acces coverage {name}")

    return coverage

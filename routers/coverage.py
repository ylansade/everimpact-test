from datetime import datetime
from json.decoder import JSONDecodeError
from typing import Annotated, List

import geojson

from fastapi import APIRouter, Depends, HTTPException, Query
from sqlalchemy.orm import Session
from geoalchemy2 import WKTElement
from shapely.geometry import shape

from database import crud, models, schemas
from utils.dependencies import get_db, authenticate_current_user, authorize_administrator, \
    authorize_coverage

router = APIRouter(
    prefix='/coverage',
    tags=['Coverage endpoints']
)

DATETIME_FORMAT = '%Y%m%d%H%M'


def get_date_range(date_begin, date_end):
    if date_begin is not None:
        try:
            date_begin = datetime.strptime(date_begin, DATETIME_FORMAT)
        except ValueError:
            raise HTTPException(status_code=400, detail='Wrong format for `date_begin`')

    if date_end is not None:
        try:
            date_end = datetime.strptime(date_end, DATETIME_FORMAT)
        except ValueError:
            raise HTTPException(status_code=400, detail='Wrong format for `date_end`')

    return date_begin, date_end


def get_polygon(polygon):
    try:
        polygon = geojson.loads(polygon)
    except JSONDecodeError:
        raise HTTPException(status_code=400, detail='Wrong format for `polygon`')

    return WKTElement(shape(polygon).wkt)


@router.post('/{name}', response_model=schemas.Coverage)
def create_coverage(name, coverage: schemas.CoverageCreate, db: Session = Depends(get_db),
                    auth: bool = Depends(authorize_administrator)):
    db_coverage = crud.get_coverage_by_name(db, name)
    if db_coverage:
        raise HTTPException(status_code=400, detail=f'Coverage {name} already exists')

    return crud.create_coverage(db, name, coverage)


@router.get('', response_model=list[schemas.Coverage])
def get_all_coverages(db: Session = Depends(get_db),
                      user: schemas.User = Depends(authenticate_current_user)):
    db_user = crud.get_user(db, user.id)
    return crud.get_coverages(db) if user.is_admin else [schemas.Coverage.from_orm(coverage)
                                                         for coverage in db_user.coverages]


@router.get('/{name}/sensors', response_model=schemas.CoverageSensors)
def get_coverage_sensors(name: str, db: Session = Depends(get_db),
                         coverage: models.Coverage = Depends(authorize_coverage)):
    return coverage


@router.get('/{name}/sinks', response_model=schemas.Coverage)
def get_coverage_sinks(name: str, db: Session = Depends(get_db),
                       coverage: models.Coverage = Depends(authorize_coverage)):
    return coverage


@router.get('/{name}/sensors/filter', response_model=List[schemas.SensorReading])
def filter_sensors(name: str, date_begin: str = None, date_end: str = None, polygon: str = None,
                   page: Annotated[int, Query(ge=1)] = 1,
                   page_size: Annotated[int, Query(lte=10000)] = 1000,
                   db: Session = Depends(get_db),
                   coverage: models.Coverage = Depends(authorize_coverage)):
    sensors_ids = None

    date_begin, date_end = get_date_range(date_begin, date_end)

    if polygon:
        polygon = get_polygon(polygon)
        sensors_ids = crud.find_sensors_by_geom(db, coverage.id, polygon)

    readings = crud.find_sensor_readings(db, date_begin, date_end, sensors_ids,
                                         (page - 1) * page_size, page_size)

    return readings


@router.get('/{name}/sinks/filter', response_model=List[schemas.Sink])
def filter_sinks(name: str, date_begin: str = None, date_end: str = None, polygon: str = None,
                 specie: str = None, age_begin: int = None, age_end: int = None,
                 page: Annotated[int, Query(ge=1)] = 1,
                 page_size: Annotated[int, Query(lte=10000)] = 1000,
                 db: Session = Depends(get_db),
                 coverage: models.Coverage = Depends(authorize_coverage)):
    date_begin, date_end = get_date_range(date_begin, date_end)

    if polygon:
        polygon = get_polygon(polygon)

    sinks = crud.find_sinks(db, coverage.id, polygon, date_begin, date_end, specie, age_begin,
                            age_end, (page - 1) * page_size, page_size)

    return sinks


@router.delete('/{name}')
def delete_coverage(name: str, db: Session = Depends(get_db),
                    auth: bool = Depends(authorize_administrator)):
    count = crud.delete_coverage_by_name(db, name)
    if count == 0:
        raise HTTPException(status_code=404,
                            detail=f'Coverage {name} was not found in the database')

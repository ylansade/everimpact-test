from typing import List

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from database import crud, models, schemas
from utils.dependencies import get_db, authenticate_current_user, authorize_administrator

router = APIRouter(
    prefix='/users',
    tags=['Users endpoints']
)


@router.post('', response_model=schemas.User)
def create_user(user: schemas.UserCreate, db: Session = Depends(get_db),
                auth: bool = Depends(authorize_administrator)):
    db_user = crud.get_user_by_name(db, user.name)
    if db_user:
        raise HTTPException(status_code=400, detail=f'User {user.name} already exists')
    return crud.create_user(db=db, user=user)


@router.get('/me', response_model=schemas.User)
def get_current_user(user: models.User = Depends(authenticate_current_user)):
    return user


@router.get('/all', response_model=List[schemas.User])
def get_all_users(db: Session = Depends(get_db), auth: bool = Depends(authorize_administrator)):
    users = crud.get_users(db)
    return users


@router.put('/me', response_model=schemas.User)
def update_current_user(update_action: schemas.UpdateUser,
                        user: models.User = Depends(authenticate_current_user),
                        db: Session = Depends(get_db)):
    coverage = crud.get_coverage(db, update_action.coverage_id)
    if not coverage:
        raise HTTPException(
            status_code=404,
            detail=f'Coverage with id {update_action.coverage_id} was not found in the database')
    return crud.update_user(db, update_action.action, user, coverage)


@router.put('/{user_id}', response_model=schemas.User)
def update_user_by_id(user_id: int, update_action: schemas.UpdateUser,
                      db: Session = Depends(get_db), auth: bool = Depends(authorize_administrator)):
    db_user = crud.get_user(db, user_id)
    if not db_user:
        raise HTTPException(status_code=404,
                            detail=f'User with id={user_id} was not found in the database')

    coverage = crud.get_coverage(db, update_action.coverage_id)
    if not coverage:
        raise HTTPException(
            status_code=404,
            detail=f'Coverage with id {update_action.coverage_id} was not found in the database')

    if update_action.action == schemas.UpdateAction.add_coverage:
        crud.add_coverage_to_user(db, db_user, coverage)
    else:
        crud.remove_coverage_from_user(db, db_user, coverage)

    return db_user


@router.delete('/{user_id}')
def delete_user(user_id: int, db: Session = Depends(get_db),
                auth: bool = Depends(authorize_administrator)):
    count = crud.delete_user(db, user_id)
    if count == 0:
        raise HTTPException(status_code=404,
                            detail=f'User with id={user_id} was not found in the database')

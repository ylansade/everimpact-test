#!/usr/bin/env python
import csv
import json
import os
import sqlite3

from datetime import datetime

import bcrypt
import pandas

from database import models


def import_users(db, csv_file):
    with open(csv_file) as users_file:
        reader = csv.DictReader(users_file)
        for user in reader:
            user['is_admin'] = bool(user['is_admin'])
            user['password'] = bcrypt.hashpw(user['password'].encode('utf8'), bcrypt.gensalt())
            db_user = models.User(**user)
            db.add(db_user)
    db.commit()


def import_coverages(db, csv_file):
    with open(csv_file) as coverages_file:
        reader = csv.DictReader(coverages_file)
        for coverage in reader:
            user_id = coverage.pop('users')
            db_coverage = models.Coverage(**coverage)
            if user_id:
                db.execute(models.user_coverage.insert().values([(int(user_id), db_coverage.id)]))
            db.add(db_coverage)
    db.commit()


def import_sensors(db, geojson_file):
    with open(geojson_file) as sensors_file:
        sensors_data = json.load(sensors_file)

    for feature in sensors_data['features']:
        geom = 'POINT(%f %f %f)' % tuple(feature['geometry']['coordinates'])
        sensor = models.Sensor(id=int(feature['properties']['Name']), geom=geom, coverage_id=1)
        db.add(sensor)

    db.commit()


def import_sensors_readings(db, csv_file):
    with open(csv_file) as readings_file:
        reader = csv.DictReader(readings_file)
        for reading in reader:
            db.add(
                models.SensorReading(
                    timestamp=datetime.fromtimestamp(float(reading['timestamp_UTC'])),
                    co2_concentration_value=int(reading['CO2_concentration_value']),
                    co2_concentration_unit=reading['CO2_concentration_unit'],
                    sensor_id=int(reading['Device_ID'])
                )
            )
    db.commit()


def import_sensors_readings_sqlite(db, sqlite_db):
    conn = sqlite3.connect(sqlite_db)

    cr = conn.cursor()
    res = cr.execute('SELECT * FROM sensorReadings')

    rows = res.fetchmany(250000)
    chunk = 1
    while rows:

        for row in rows:
            db.add(
                models.SensorReading(
                    timestamp=datetime.fromtimestamp(float(row[3])),
                    co2_concentration_value=row[15],
                    co2_concentration_unit=row[16],
                    sensor_id=row[5]
                )
            )

        db.commit()
        print(f'sensor reading chunk #{chunk} imported')
        chunk += 1

        rows = res.fetchmany(250000)


def import_sinks(db, csv_file):
    chunk = 1
    for df in pandas.read_csv(csv_file, chunksize=250000):

        for index, sink in df.iterrows():
            points = sink['geometry'].split('; ')
            geom = 'POLYGON((%s))' % ','.join(points)
            db.add(
                models.Sink(
                    parcel_id=sink['parcel_id'],
                    date=datetime.strptime(sink['Date'], '%Y-%m-%d %H:%M:%S,%f'),
                    specie=sink['Specie'],
                    age=int(sink['Age']),
                    co2removed=float(str(sink['co2removed']).replace(',', '.')),
                    co2emitted=float(str(sink['co2emitted']).replace(',', '.')),
                    co2balance=float(str(sink['co2balance']).replace(',', '.')),
                    geom=geom,
                    coverage_id=2,
                )
            )

        db.commit()
        print(f'sinks chunk #{chunk} imported')

        chunk += 1


def init_test_db(db):
    import_users(db, 'data/users.csv')
    import_coverages(db, 'data/coverages.csv')
    import_sensors(db, 'data/sensors_locations.geojson')
    import_sensors_readings(db, 'data/sensorReadings.csv')
    import_sinks(db, 'data/sinks.csv')


def init_db(db):
    print('importing users')
    import_users(db, 'data/users.csv')
    print('importing coverages')
    import_coverages(db, 'data/coverages.csv')
    print('importing sensors')
    import_sensors(db, 'data/sensors_locations.geojson')

    print('importing sensor readings')
    if os.path.exists('data/Dijon_sensorSQLite20220530.db'):
        import_sensors_readings_sqlite(db, 'data/Dijon_sensorSQLite20220530.db')
    else:
        import_sensors_readings(db, 'data/sensorReadings.csv')

    print('importing sinks')
    if os.path.exists('data/Ishinomaki_DB.csv'):
        import_sinks(db, 'data/Ishinomaki_DB.csv')
    else:
        import_sinks(db, 'data/sinks.csv')


if __name__ == "__main__":
    from database.engine import engine, SessionLocal

    models.Base.metadata.create_all(bind=engine)
    db = SessionLocal()
    init_db(db)

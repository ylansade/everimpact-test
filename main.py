from fastapi import Depends, FastAPI

from database import models
from database.engine import engine
from routers import coverage, users
from utils.dependencies import authenticate_current_user

models.Base.metadata.create_all(bind=engine)

app = FastAPI(dependencies=[Depends(authenticate_current_user)])

app.include_router(coverage.router)
app.include_router(users.router)

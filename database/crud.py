import bcrypt

from datetime import datetime
from typing import List

from sqlalchemy.orm import Session
from sqlalchemy.sql import func
from geoalchemy2 import WKTElement

from . import models, schemas


def create_user(db: Session, user: schemas.UserCreate):
    user.password = user.password.encode('utf8') if type(user.password) == str else user.password
    user.password = bcrypt.hashpw(user.password, bcrypt.gensalt())
    db_user = models.User(**dict(user))
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def get_user(db: Session, user_id: int):
    return db.query(models.User).filter(models.User.id == user_id).first()


def get_user_by_name(db: Session, name: str):
    return db.query(models.User).filter(models.User.name == name).first()


def get_users(db: Session):
    return db.query(models.User).all()


def add_coverage_to_user(db: Session, user: models.User, coverage: models.Coverage):
    if coverage not in user.coverages:
        user.coverages.append(coverage)
    db.commit()
    db.refresh(user)


def remove_coverage_from_user(db: Session, user: models.User, coverage: models.Coverage):
    if coverage in user.coverages:
        user.coverages.remove(coverage)
    db.commit()
    db.refresh(user)


def update_user(db: Session, action: schemas.UpdateAction, user: models.User,
                coverage: models.Coverage):
    if action == schemas.UpdateAction.add_coverage:
        add_coverage_to_user(db, user, coverage)
    elif action == schemas.UpdateAction.remove_coverage:
        remove_coverage_from_user(db, user, coverage)
    return user


def delete_user(db: Session, user_id: int):
    count = db.query(models.User).filter(models.User.id == user_id).delete()
    db.commit()
    return count


def create_coverage(db: Session, name: str, coverage: schemas.CoverageCreate):
    params = dict(coverage)
    params['name'] = name
    db_coverage = models.Coverage(**params)
    db.add(db_coverage)
    db.commit()
    db.refresh(db_coverage)
    return db_coverage


def get_coverage(db: Session, coverage_id: int):
    return db.query(models.Coverage).filter(models.Coverage.id == coverage_id).first()


def get_coverage_by_name(db: Session, name: str):
    return db.query(models.Coverage).filter(models.Coverage.name == name).first()


def get_coverages(db: Session):
    return db.query(models.Coverage).all()


def delete_coverage_by_name(db: Session, name: str):
    count = db.query(models.Coverage).filter(models.Coverage.name == name).delete()
    db.commit()
    return count


def find_sensors_by_geom(db: Session, coverage_id: int, geom: WKTElement):
    return [row[0] for row in
            db.query(models.Sensor).filter(
                models.Sensor.coverage_id == coverage_id,
                func.ST_Contains(geom, models.Sensor.geom)
            ).with_entities(models.Sensor.id).all()]


def find_sensor_readings(db: Session, date_begin: datetime, date_end: datetime,
                         sensors_ids: List[int], offset: int = 0, limit: int = 1000):
    # sensor_ids is None if there's no polygon param so we can go on
    # if it's an empty list, there are no sensors associated with the
    # geometry used to filter result so there are no readings
    if sensors_ids is not None and not len(sensors_ids):
        return []

    query = db.query(models.SensorReading)

    if date_begin:
        query = query.filter(date_begin <= models.SensorReading.timestamp)

    if date_end:
        query = query.filter(date_end >= models.SensorReading.timestamp)

    if sensors_ids:
        query = query.filter(models.SensorReading.sensor_id.in_(sensors_ids))

    return query.offset(offset).limit(limit).all()


def find_sinks(db: Session, coverage_id: int, geom: WKTElement = None, date_begin: datetime = None,
               date_end: datetime = None, specie: str = None, age_begin: int = None,
               age_end: int = None, offset: int = 0, limit: int = 1000):
    query = db.query(models.Sink).filter(models.Sink.coverage_id == coverage_id)

    if geom:
        query = query.filter(
            func.ST_Intersects(models.Sink.geom, geom)
        )

    if date_begin:
        query = query.filter(date_begin <= models.Sink.date)

    if date_end:
        query = query.filter(date_end >= models.Sink.date)

    if specie:
        query = query.filter(specie == models.Sink.specie)

    if age_begin:
        query = query.filter(age_begin <= models.Sink.age)

    if age_end:
        query = query.filter(age_end >= models.Sink.age)

    return query.offset(offset).limit(limit).all()

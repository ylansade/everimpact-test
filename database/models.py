import json

from datetime import datetime
from typing import List

import shapely

from sqlalchemy import Boolean, Column, Float, ForeignKey, String, Table
from sqlalchemy.orm import Mapped, mapped_column, relationship
from geoalchemy2 import Geometry
from geoalchemy2.shape import to_shape

from .engine import Base


user_coverage = Table(
    'user_coverage',
    Base.metadata,
    Column('user_id', ForeignKey('users.id'), primary_key=True),
    Column('coverage_id', ForeignKey('coverages.id'), primary_key=True)
)


class User(Base):
    __tablename__ = 'users'

    id: Mapped[int] = mapped_column(primary_key=True)
    name = Column(String, unique=True, index=True, nullable=False)
    password = Column(String, nullable=False)
    is_admin = Column(Boolean, nullable=False, default=False)

    coverages: Mapped[List['Coverage']] = relationship(
        secondary=user_coverage, back_populates='users'
    )


class Coverage(Base):
    __tablename__ = 'coverages'

    id: Mapped[int] = mapped_column(primary_key=True)
    name = Column(String, unique=True, index=True, nullable=True)
    spatial_resolution = Column(Float)
    temporal_resolution = Column(Float)
    depth = Column(Float)

    users: Mapped[List[User]] = relationship(
        secondary=user_coverage, back_populates='coverages'
    )
    sensors: Mapped[List['Sensor']] = relationship(back_populates='coverage')
    sinks: Mapped[List['Sink']] = relationship(back_populates='coverage')


class Sensor(Base):
    __tablename__ = 'sensors'

    id: Mapped[int] = mapped_column(primary_key=True)
    geom = Column(Geometry(geometry_type='POINT', dimension=3, management=True), nullable=False)

    coverage_id: Mapped[int] = mapped_column(ForeignKey('coverages.id'), nullable=False)
    coverage: Mapped['Coverage'] = relationship(back_populates='sensors')

    @property
    def point(self):
        return json.loads(shapely.to_geojson(to_shape(self.geom)))


class SensorReading(Base):
    __tablename__ = 'sensor_readings'

    id: Mapped[int] = mapped_column(primary_key=True)
    timestamp: Mapped[datetime] = mapped_column(index=True)

    co2_concentration_value: Mapped[int]
    co2_concentration_unit: Mapped[str]

    sensor_id: Mapped[int] = mapped_column(ForeignKey('sensors.id'), nullable=False, index=True)
    sensor: Mapped['Sensor'] = relationship()


class Sink(Base):
    __tablename__ = 'sinks'

    id: Mapped[int] = mapped_column(primary_key=True)
    parcel_id: Mapped[str]
    date: Mapped[datetime] = mapped_column(index=True)
    specie: Mapped[str] = mapped_column(index=True)
    age: Mapped[int] = mapped_column(index=True)
    co2removed: Mapped[float]
    co2balance: Mapped[float]
    co2emitted: Mapped[float]
    geom = Column(Geometry(geometry_type='Polygon', management=True), nullable=False)

    coverage_id: Mapped[int] = mapped_column(ForeignKey('coverages.id'), nullable=False)
    coverage: Mapped['Coverage'] = relationship(back_populates='sinks')

    @property
    def polygon(self):
        return json.loads(shapely.to_geojson(to_shape(self.geom)))

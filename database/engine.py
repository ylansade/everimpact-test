import os

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.event import listen
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import select, func


def load_spatialite(dbapi_conn, connection_record):
    dbapi_conn.enable_load_extension(True)
    dbapi_conn.load_extension('/usr/lib/x86_64-linux-gnu/mod_spatialite.so')


SQLALCHEMY_DATABASE_URL = 'sqlite:///database.db'

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={'check_same_thread': False},
)

listen(engine, 'connect', load_spatialite)
if not os.path.exists('database.db'):
    # the command always returns an error so we wrap it in a try/except
    conn = engine.connect()
    try:
        conn.execute(select([func.InitSpatialMetaData()]))
    except Exception:
        pass
    finally:
        conn.close()

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()

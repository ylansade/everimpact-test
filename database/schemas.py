from datetime import datetime
from typing import List, Optional, Tuple, Union
from enum import Enum

from pydantic import BaseModel, Field


class Point(BaseModel):
    type: str = Field('Point', const=True)
    coordinates: Union[Tuple[float, float], Tuple[float, float, float]]


class Polygon(BaseModel):
    type: str = Field('Polygon', const=True)
    coordinates: List[List[Tuple[float, float]]]


class CoverageBase(BaseModel):
    name: str


class CoverageCreate(BaseModel):
    spatial_resolution: Optional[float]
    temporal_resolution: Optional[float]
    depth: Optional[float]


class Coverage(CoverageCreate):
    id: int
    name: str

    class Config:
        orm_mode = True


class CoverageNested(CoverageBase):
    id: int

    class Config:
        orm_mode = True


class Sensor(BaseModel):
    id: int
    point: Point

    class Config:
        orm_mode = True


class CoverageSensors(Coverage):
    sensors: List[Sensor]


class UserBase(BaseModel):
    name: str
    is_admin: bool = True


class UserCreate(UserBase):
    password: str


class User(UserBase):
    id: int
    coverages: list[CoverageNested] = []

    class Config:
        orm_mode = True


class UpdateAction(Enum):
    add_coverage = 'add_coverage'
    remove_coverage = 'remove_coverage'


class UpdateUser(BaseModel):
    action: UpdateAction
    coverage_id: int


class SensorReading(BaseModel):
    timestamp: datetime
    co2_concentration_value: int
    co2_concentration_unit: str
    sensor_id: int

    class Config:
        orm_mode = True


class Sink(BaseModel):
    parcel_id: str
    date: datetime
    specie: str
    co2removed: float
    co2balance: float
    co2emitted: float
    age: int
    polygon: Polygon

    class Config:
        orm_mode = True

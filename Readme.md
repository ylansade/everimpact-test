# Carbon Site Monitoring API

## Setting up dev environment

### Installing dependencies

Run `python -m venv venv` to create a venv.

Run `source venv/bin/activate` to activate the venv.

Run `pip install -r requirements.txt -r requirements-dev.txt` to install
dependencies.

You need spatialite for the application to run. On debian based systems, run
`sudo apt install libsqlite3-mod-spatialite`.

### Database

The database is located in the file `database.db`. To initialize it,
run `./init_db.py`.

This will create the database and insert initial data into it.

### Running the app

Use the command `uvicorn main:app` to run the app.

### Running the unit tests

Run `python -m unittest` to run the tests.

### Documentation of the api

The documentation of the api is available at http://127.0.0.1/docs when the app
is running.

## Running the app with docker

Build the image with: `docker build -t <image_name> .`.

Run it with: `docker run -p 8000:8000 <image_name>`.

The api will be available on http://127.0.0.1 with the documentation available
on http://127.0.0.1/docs

### Running the app in docker with provided data

The `init_db.py` script looks for the files `Dijon_sensorSQLite20220530.db` and
`Ishinomaki_DB.csv` in the `data/` directory. If you put those files there, the
database in the docker image with be initialized with data from those files.

*Warning:* the dataset is quite large, it takes a while to load everything in
the database.
